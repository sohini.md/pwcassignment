import axios from "axios";

const API_URL = "https://fakestoreapi.com/";

const getPublicContent = () => {
  return axios.get(API_URL + "all");
};
const userService = {
  getPublicContent
};

export default userService