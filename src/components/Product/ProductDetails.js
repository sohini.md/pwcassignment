import React, { useEffect, useState } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Modal, Button } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  selectedProduct
} from "../../redux/actions/productsActions";
import {addItem, delItem} from '../../redux/actions/cartActions'

const ProductDetails = () => {
  const [show, setShow] = useState(false);
  const [totalPrice, setTotalPrice] = useState(0);
  const [cartBtn, setCartBtn] = useState("Add to Cart");
  const { productId } = useParams();
  let product = useSelector((state) => state.reducers.product);
  const { image, title, price, category, description, rating } = product;
  const dispatch = useDispatch();
  const fetchProductDetail = async (id) => {
    const response = await axios
      .get(`https://fakestoreapi.com/products/${id}`)
      .catch((err) => {
        console.log("Err: ", err);
      });
    dispatch(selectedProduct(response.data));
  };
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleCart = (product) => {
    if (cartBtn === "Add to Cart") {
      dispatch(addItem(product))
      setCartBtn("Remove from Cart")
  }
  else{
      dispatch(delItem(product))
      setCartBtn("Add to Cart")
  }
  }
  useEffect(() => {
    if (productId && productId !== "") {
      fetchProductDetail(productId);
    }
  }, [productId]);

  return (
    <div className="ui grid container">
      {Object.keys(product).length === 0 ? (
        <div>...Loading</div>
      ) : (
        <>
          <div>
            <div className="ui two column stackable center aligned grid">
              <div className="middle aligned row">
                <div className="column lp productdetailsimage">
                  <img className="ui fluid image" src={image} />
                </div>
                <div className="column rp">
                  <h1>{title}</h1>
                  <h2>
                    <span className="ui teal tag label">${price}</span>
                  </h2>
                  <h3 className="ui brown block header">{category}</h3>
                  <p>{description}</p>
                  <div className="ui vertical animated button" tabIndex="0">
                    <div className="hidden content">
                      <i className="shop icon"></i>
                    </div>
                    <div className="cartbuttondetails" onClick={handleShow}>Add to Cart</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Order Details</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="ui two column stackable center aligned grid">
                <div className="middle aligned row">
                  <div className="column lp modalimage">
                    <img className="ui fluid image" src={image} />
                  </div>
                  <div className="column rp">
                    <p>{title}</p>
                    <h2>
                      <span className="modalprice">Product Price: ${price}</span>
                    </h2>
                    <h3 className="modalprice">Quantity: {rating.count}</h3>
                    <div className="ui vertical animated button" tabIndex="0">
                      <div className="hidden content">
                        <i className="shop icon"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <div>Product Total: ${totalPrice ? totalPrice : price}</div>
              <div className="cartbuttondetails" onClick={()=>handleCart(product)} >{cartBtn}</div>
            </Modal.Footer>
          </Modal>
        </>
      )}
    </div>
  );
};

export default ProductDetails;