import React, { useState, useEffect } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import 'font-awesome/css/font-awesome.min.css';
import "bootstrap/dist/css/bootstrap.min.css";
import { setProducts } from "../../redux/actions/productsActions";
import Product from "./Product";
import ProductListView from "./ProductListView";

const ProductPage = () => {
  const [category, setCategory] = useState([]);
  const [cat, setCat] = useState('');
  const [selectedPage, setSelectedPage] = useState('Grid');
  const products = useSelector((state) => state.reducers.allProducts?.products);
  const dispatch = useDispatch();
  const fetchProducts = async () => {
    let url = 'https://fakestoreapi.com/products';
    if (cat) {
      url = 'https://fakestoreapi.com/products/category/' + cat;
    }
    const response = await axios
      .get(url)
      .catch((err) => {
        console.log("Err: ", err);
      });

    dispatch(setProducts(response.data));
  };
  const fetchCategory = async () => {
    const response = await axios
      .get("https://fakestoreapi.com/products/categories")
      .catch((err) => {
        console.log("Err: ", err);
      });
    setCategory(response.data);
  };
  useEffect(() => {
    fetchProducts();
    fetchCategory();
  }, [cat]);

  const setcategoryleft = (cat) => {
    setCat(cat);
  }
  const selectedView = (view) => {
    setSelectedPage(view);
  }
  return (
    <div className="ui grid container">
      <div className="leftpanel">
        <p className="paneltitle">Sort By Relevance <i className='fa fa-angle-right rightwhiteicon' ></i></p>
        {category.map((cat, id) => {
          return <p><span onClick={() => setcategoryleft(cat)}>{cat}</span> <span className="caticon"><i className='fa fa-angle-right righticon' ></i></span></p>
        })}
      </div>
      <div className="rightpanel">
        <div className="headerRightproduct">
          <span className="headLeft">All Products {products.length}</span>
          <span className="headRight">
            <span className="headRightList" onClick={() => selectedView('List')}><i className={selectedPage === 'List' ? 'fa fa-list-ul selectedListIcon' : 'fa fa-list-ul'} aria-hidden="true"></i></span>
            <span className="headRightGrid" onClick={() => selectedView('Grid')}><i className={selectedPage === 'Grid' ? 'fa fa-th-large selectedGridIcon' : 'fa fa-th-large'} aria-hidden="true"></i></span>
          </span>
        </div>
        {selectedPage === 'Grid' && <Product />}
        {selectedPage === 'List' && <ProductListView />}
      </div>
    </div>
  );
};

export default ProductPage;