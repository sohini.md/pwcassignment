import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

const Product = () => {
  const products = useSelector((state) => state.reducers.allProducts?.products);

  const renderList = products?.map((product) => {
    const { id, title, image, price, category } = product;
    return (
      <div className="four wide column" key={id}>
        <Link to={`/product/${id}`}>
          <div className="ui link cards">
            <div className="card">
              <div className="image">
                <img src={image} alt={title} />
              </div>
              <div className="content">
                <div className="header">
                  {title.substring(0, 20)} {title.length >= 20 && '...'}
                </div>
                <div className="meta price">$ {price}</div>
                <div className="meta">{category}</div>
                <div className="cartbutton">
                  <button type="submit" className="btn btn-primary btn-block">
                    <span>ADD</span>
                  </button></div>
              </div>
            </div>
          </div>
        </Link>
      </div>
    );
  });
  return <>{renderList}</>;
};

export default Product;