import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

const ProductListView = () => {
  const products = useSelector((state) => state.reducers.allProducts?.products);
  const renderList = products?.map((product) => {
    const { id, title, image, price, category } = product;
    return (
      <div className="four wide column" key={id}>

        <div className="">
          <div className="cardList">
            <div className="image">
              <img src={image} alt={title} />
            </div>
            <div className="contentList">
              <div className="header">
                {title}
              </div>
              <div className="meta pricelist">$ {price}</div>
              <div className="metalist">{category}</div>
              <div className="cartbuttonList">
                <Link to={`/product/${id}`}>
                  <button type="submit" className="btn btn-primary btn-block">
                    <span>ADD</span>
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  });
  return <>{renderList}</>;
};

export default ProductListView;