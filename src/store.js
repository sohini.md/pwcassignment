import { configureStore } from '@reduxjs/toolkit'
import authReducer from "./slices/auth";
import messageReducer from "./slices/message";
import reducers from "./redux/reducers/index";

const reducer = {
  auth: authReducer,
  message: messageReducer,
  reducers
}

const store = configureStore({
  reducer: reducer,
  devTools: true,
})

export default store;