import React, { useState, useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter as Router, Routes, Route, Link, NavLink } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import 'font-awesome/css/font-awesome.min.css';
import ProductListing from "./components/Product/ProductListing";
import ProductDetails from "./components/Product/ProductDetails";
import Cart from "./components/Product/Cart";
import Checkout from "./components/Product/Checkout";
import { logout } from "./slices/auth";
import EventBus from "./common/EventBus";
import "./App.css";
import Login from "./components/Login/Login";

const App = () => {
  const { user: currentUser } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const logOut = useCallback(() => {
    dispatch(logout());
  }, [dispatch]);

  useEffect(() => {
    EventBus.on("logout", () => {
      logOut();
    });

    return () => {
      EventBus.remove("logout");
    };
  }, [currentUser, logOut]);
  const state = useSelector((state) => state);
  return (
    <Router>
      <div>
        <p className="title-header">ZELDA</p>
        {currentUser && (
          <div className="header-right">
            <div className="right-links">
              <a href="#"><i className="fa fa-search"></i></a>
            </div>
            <div className="right-links">
              <NavLink to="/cart" className="">
                <i className="fa shoppingCart">&#xf07a;</i>
                <span className='badge badge-warning lblCartCount'> {state.reducers.cartReducer.length} </span>
              </NavLink>
            </div>
          </div>
        )}
        <nav className="navbar navbar-default center navbar-expand navbar-dark header-app">
          {currentUser ? (
            <>
              <div className="navbar-nav mx-auto">
                <li className="nav-item">
                  <Link to={`/product/`} className="nav-link">
                    PRODUCTS
                  </Link>
                </li>
              </div>
              <div className="navbar-nav mx-auto">
                <li className="nav-item">
                  <a href="/login" className="nav-link">
                    EDUCATION
                  </a>
                </li>
              </div>
              <div className="navbar-nav mx-auto">
                <li className="nav-item">
                  <a href="/login" className="nav-link" >
                    ORDERS
                  </a>
                </li>
              </div>
              <div className="navbar-nav mx-auto">
                <li className="nav-item">
                  <a href="/login" className="nav-link" onClick={logOut}>
                    LogOut
                  </a>
                </li>
              </div>
            </>
          ) : (
            <div className="navbar-nav mx-auto">
              <li className="nav-item">
                <Link to={"/login"} className="nav-link">
                  Login
                </Link>
              </li>
            </div>
          )}
        </nav>

        <div className="container mt-3 mainContainer">
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/product" element={<ProductListing />} />
            <Route path="/product/:productId" element={<ProductDetails />} />
            <Route path="/cart" element={<Cart />} />
            <Route path="/checkout" element={<Checkout />} />
          </Routes>
        </div>
      </div>
    </Router>
  );
};

export default App;